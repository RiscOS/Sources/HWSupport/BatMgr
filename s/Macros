; Copyright 1996 Acorn Computers Ltd
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
;     http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.
;
        SUBT    Macros -> Source.Macros

; Change record
; =============
;
; CDP - Christopher Partington, Cambridge Systems Design
;
;
; 11-Dec-91  18:06  CDP
; First version.
;
; 11-Mar-92  10:44  CDP
; Debug macros removed.
;
;
;*End of change record*

;******************************************************************************

; Declare word-aligned space
;
        MACRO
$lab    a4      $size           ;allocate word aligned register relative
        ASSERT  (:INDEX: {VAR}) :MOD: 4=0
$lab    #       $size
        MEND


;******************************************************************************

; Macro to load an immediate value into a register
; Using in order of preference:
;
;    1. MOV     Rn,#x
;
;    2. MOV     Rn,#m
;       ORR     Rn,Rn,#n
;
;    3. MOV     Rn,#m
;       BIC     Rn,Rn,#n
;
;    4. LDR     Rn,=x

                GBLL    ldimm_verbose1
                GBLL    ldimm_verbose2
ldimm_verbose1  SETL    {FALSE}
ldimm_verbose2  SETL    {FALSE}

        MACRO
        LDIMM   $reg,$var,$cc
        LCLA    shift
        LCLA    varcopy
        LCLA    tmp
varcopy SETA    $var
shift   SETA    0
        WHILE   ((varcopy:AND:&FFFFFF00) <> 0) :LAND: (shift <= 30)
shift   SETA    shift + 2
varcopy SETA    varcopy:ROR:2
        WEND
        [       (varcopy:AND:&FFFFFF00) = 0
        MOV$cc  $reg,#$var
        MEXIT
        ]
varcopy SETA    $var
shift   SETA    0
        WHILE   ((varcopy:AND:&FFFF0000) <> 0) :LAND: (shift <= 30)
shift   SETA    shift + 2
varcopy SETA    varcopy:ROR:2
        WEND
        [       (varcopy:AND:&FFFF0000) = 0
tmp     SETA    $var :AND: (&FF :ROL: shift)
        MOV$cc  $reg,#&$tmp
tmp     SETA    $var :AND: (&FF00 :ROL: shift)
        ORR$cc  $reg,$reg,#&$tmp
        [       ldimm_verbose1
tmp     SETA    $var
        !       0,"LDIMM $reg,$var (&$tmp) needed MOV/ORR"
        ]
        MEXIT
        ]
varcopy SETA    $var
shift   SETA    0
        WHILE   ((varcopy:OR:&FF) <> &FFFFFFFF) :LAND: (shift <= 30)
shift   SETA    shift + 2
varcopy SETA    varcopy:ROR:2
        WEND
        [       (varcopy:OR:&FF) = &FFFFFFFF
        MOV$cc  $reg,#$var
        MEXIT
        ]
varcopy SETA    $var
shift   SETA    0
        WHILE   ((varcopy:OR:&FFFF) <> &FFFFFFFF) :LAND: (shift <= 30)
shift   SETA    shift + 2
varcopy SETA    varcopy:ROR:2
        WEND
        [       (varcopy:OR:&FFFF) = &FFFFFFFF
tmp     SETA    (varcopy:OR:&FF) :ROL: shift
        MOV$cc  $reg,#&$tmp
tmp     SETA    (:NOT:(varcopy:OR:&FF00)) :ROL: shift
        BIC$cc  $reg,$reg,#&$tmp
        [       ldimm_verbose1
tmp     SETA    $var
        !       0,"LDIMM $reg,$var (&$tmp) needed MOV/BIC"
        ]
        MEXIT
        ]
        LDR$cc  $reg,=$var
        [       ldimm_verbose2
tmp     SETA    $var
        !       0,"LDIMM $reg,$var (&$tmp) needed LDR"
        ]
        MEXIT
        MEND

;******************************************************************************

        END
